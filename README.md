# ShaderGen

ShaderGen is a small code generator for generating shader code for [3Delight for cinema 4D](https://gitlab.com/FilipMalmberg/3delight-for-cinema-4d) from compiled OSL shaders. Given an OSO-file, ShaderGen creates:

- C++ code for the appropriate Cinema4D node
- Resource files for the Cinema4D node GUI
- C++ code for a translator plugin, that translates the cinema4D node to an appropriate NSI node for rendering in 3Delight.

Generating this code automatically from compiled OSL shaders avoids the tedious process of writing and maintaining shader translation code, and ensures that UI widgets and translation logic is consistent across all shaders.

# Building (windows)
- Make sure you have a current and working [3Delight](https://www.3delight.com/download) installation.
- Download the ShaderGen source code
- Create a folder called "build" in the "shadergen" directory
- Open a command prompt and navigate to the newly created "build" directory
- Run the following command in the prompt "cmake .. -A x64" to generate a Visual Studio project, located in the "build" folder
- Launch Visual Studio and compile the project

# Usage
The following command generates code and resource files for a compiled OSL shader called "MyShader.oso":

ShaderGen /osl/MyShader.oso

The compiled shader is assumed to be located within the 3Delight installation folder.