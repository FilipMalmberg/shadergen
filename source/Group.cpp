#include "Group.h"
#include <iostream>
#include <string>

#include "CodeGen.h"

using namespace std;
string Ind(int level) {
	string t = "";
	for (int i = 0; i < level; i++) {
		t = t + "\t";
	}
	return t;
}

void Group::WriteDescriptions(DlShaderInfo* shaderinfo, ofstream& file, int indent ) {
	file << Ind(indent)<< "GROUP "<<name << " {" << endl;
	file << Ind(indent) << "DEFAULT 1;" << endl;
	
	for (int i = 0; i < subgroups.size(); i++) {
		subgroups[i].WriteDescriptions(shaderinfo, file, indent+1);
	}

	for (int i = 0; i < parameters.size(); i++) {
		//cout << Ind(indent+1) << "Parameter " << parameters[i] << endl;
		WriteParameterDescription(shaderinfo, parameters[i], file, indent+1);
	}
	file << Ind(indent) << "}" << endl;
}

void Group::InsertInSubgroup(int parameter, vector<string>& subgroup_name) {
	if (subgroup_name.size() == 0) {
		parameters.push_back(parameter);
	}
	else {
		int g=0;
		bool found = false;
		for (int i = 0; i < subgroups.size() && !found; i++) {
			if(subgroups[i].name==GroupName(subgroup_name.back())){
				found = true;
				g = i;
			}
		}

		if (!found) {
			Group new_group;
			new_group.name = GroupName(subgroup_name.back());
			subgroups.push_back(new_group);
			g = subgroups.size() - 1;
		}
		subgroup_name.pop_back();
		subgroups[g].InsertInSubgroup(parameter, subgroup_name);
	}
}

void Group::AddParameter(int p) {
	parameters.push_back(p);
}