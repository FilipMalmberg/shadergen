#pragma once

#include "ShaderQuery.h"
#include "nsi.h"
#include "Group.h"
#include<iostream>
#include <string>
#include <fstream>
#include <filesystem>
#include <algorithm>
#include <set>
#include <cctype>
#include <sstream>
#include <vector>

using namespace std;

namespace fs = std::filesystem;
using namespace std;


std::string ClassName(string shadername);

std::string str_toupper(std::string s);

std::string str_tolower(std::string s);

std::string space2underscore(std::string text);

std::string EnumName(string param_name);
std::string ParamName(string param_name);
std::string IdName(string param_name);
std::string GroupName(string page_name);

//void WriteTabs(ofstream& os, int ntabs);

string Indent(int level);

std::vector<std::string> SplitString(const std::string &s, char delim);

string GetNiceName(DlShaderInfo* shaderinfo);

void GetGroups(DlShaderInfo* shaderinfo);

void WriteCppFile(DlShaderInfo* shaderinfo);
void WriteTranslator(DlShaderInfo* shaderinfo, string shaderpath);
void WriteHeaderFile(DlShaderInfo* shaderinfo);
void WriteParameterDescription(DlShaderInfo* shaderinfo, int i, ofstream& file, int indent);
void WriteResourceFiles(DlShaderInfo* shaderinfo);

