#pragma once
#include <string>
#include <vector>
using namespace std;

class Parameter {
public:

	string name;
	int type;
	string enum_name;
	bool is_closure;

	//Metadata
	string default_connection;
	int skip_init;
	string help;
	string label;
	float min;
	float max;
	//int min;
	//int max;
	string niceName;
	string options;
	string page;
	float slidermin;
	float slidermax;
	//int slidermin;
	//int slidermax;
	int texturefile;
	string widget;












	//These are for shaders, not parameters:
	//string niceName; 
	//vector<string> tags;

};