#define _CRT_SECURE_NO_WARNINGS
#include "CodeGen.h"
#include "osl_utilities.h"
#include <map>
using namespace std;

Group ParentGroup;
map<string, string> custom_translation_code;


std::string ClassName(string shadername) {
	shadername[0] = std::toupper(shadername[0]);
	return shadername;
}

std::string str_toupper(std::string s)
{
	std::transform(s.begin(), s.end(), s.begin(),
		[](unsigned char c) { return std::toupper(c); }
	);
	return s;
}

std::string str_tolower(std::string s)
{
	std::transform(s.begin(), s.end(), s.begin(),
		[](unsigned char c) { return std::tolower(c); }
	);
	return s;
}

std::string space2underscore(std::string text) {
	std::replace(text.begin(), text.end(), ' ', '_');
	return text;
}

std::string ReplaceTokens(string s) {
	std::replace(s.begin(), s.end(), '/', '_');
	std::replace(s.begin(), s.end(), '.', '_');
	std::replace(s.begin(), s.end(), '(', '_');
	std::replace(s.begin(), s.end(), ')', '_');
	std::replace(s.begin(), s.end(), '-', '_');
	std::replace(s.begin(), s.end(), ' ', '_');
	std::replace(s.begin(), s.end(), '+', 'P');
	return s;
}

std::string EnumName(string param_name) {
	/*std::replace(param_name.begin(), param_name.end(), '/', '_');
	std::replace(param_name.begin(), param_name.end(), '.', '_');
	std::replace(param_name.begin(), param_name.end(), '(', '_');
	std::replace(param_name.begin(), param_name.end(), ')', '_'); 
	std::replace(param_name.begin(), param_name.end(), '-', '_');*/
	param_name = ReplaceTokens(param_name);
	return str_toupper(param_name);
}

std::string ParamName(string param_name) {
	/*std::replace(param_name.begin(), param_name.end(), '/', '_');
	std::replace(param_name.begin(), param_name.end(), '.', '_');
	std::replace(param_name.begin(), param_name.end(), '(', '_');
	std::replace(param_name.begin(), param_name.end(), ')', '_');
	std::replace(param_name.begin(), param_name.end(), '-', '_');*/
	param_name = ReplaceTokens(param_name);
	return str_toupper("PARAM_" + param_name);
}

std::string IdName(string param_name) {
	/*std::replace(param_name.begin(), param_name.end(), '/', '_');
	std::replace(param_name.begin(), param_name.end(), '.', '_');
	std::replace(param_name.begin(), param_name.end(), '(', '_');
	std::replace(param_name.begin(), param_name.end(), ')', '_');
	std::replace(param_name.begin(), param_name.end(), '-', '_');*/
	param_name = ReplaceTokens(param_name);
	return str_toupper("ID_" + param_name);
}

std::string GroupName(string page_name) {
	//std::replace(page_name.begin(), page_name.end(), '/', '_');
	//return str_toupper(space2underscore(page_name));
	page_name = ReplaceTokens(page_name);
	return "GROUP_" + str_toupper(page_name);
}

void WriteTabs(ofstream& os, int ntabs) {
	for (int i = 0; i < ntabs; i++) {
		os << "\t";
	}
}

string Indent(int level) {
	string t = "";
	for (int i = 0; i < level; i++) {
		t = t + "\t";
	}
	return t;
}

std::vector<std::string> SplitString(const std::string &s, char delim) {
	std::vector<std::string> result;
	std::stringstream ss(s);
	std::string item;

	while (getline(ss, item, delim)) {
		result.push_back(item);
	}

	return result;
}



void GetGroups(DlShaderInfo* shaderinfo) {
	//Group g;
	ParentGroup.name = "ID_GVPROPERTIES";

	long nparams = shaderinfo->nparams();


	for (long i = 0; i < nparams; i++) {
		auto param = shaderinfo->getparam(i);
		auto metadata = param->metadata;

		bool page_found = false;
		string page;
		for (long j = 0; j < metadata.size() && !page_found; j++) {
			DlShaderInfo::Parameter meta = metadata[j];
			if (meta.name == "page" && meta.sdefault.size() > 0) {
				page = string(meta.sdefault[0].c_str());
				//page = EnumName(page);
				vector<string> pages = SplitString(page, '.');
				std::reverse(pages.begin(), pages.end());

				ParentGroup.InsertInSubgroup(i, pages);
				/*for (int k = 0; k < pages.size(); k++) {
					groups.insert(pages[k]);
				}*/


				page_found = true;
			}
		}
		if (!page_found) {
			ParentGroup.AddParameter(i);
		}

	}
}


/*bool Has_C4d_ID(DlShaderInfo* shaderinfo){
	auto metadata = shaderinfo->metadata();

	bool has_c4d_ID = false;

	for (long i = 0; i < metadata.size() && !has_c4d_ID; i++) {
		DlShaderInfo::Parameter meta = metadata[i];
		if (meta.name == "c4d_ID" && meta.type==NSITypeInteger ) {
			has_c4d_ID = true;
		}

	}
	return has_c4d_ID;

}*/


//Check for "c4d_type" metadata entry to determine if the shader is a material. 
/*bool IsMaterial(DlShaderInfo* shaderinfo) {
	bool isMaterial = false;
	auto metadata = shaderinfo->metadata();

	for (long i = 0; i < metadata.size() && !isMaterial; i++) {
		DlShaderInfo::Parameter meta = metadata[i];
		if (meta.name == "c4d_type" && meta.sdefault.size() > 0) {
			string type = string(meta.sdefault[0].c_str());
			if (type == "material") {
				isMaterial = true;
			}
		}
	}

	return isMaterial;

}*/

string GetNiceName(DlShaderInfo* shaderinfo) {
	string name = string(shaderinfo->shadername().c_str()); //Default, nice name=name
	bool name_found = false;

	auto metadata = shaderinfo->metadata();
	for (long i = 0; i < metadata.size() && !name_found; i++) {
		DlShaderInfo::Parameter meta = metadata[i];
		if (meta.name == "niceName" && meta.sdefault.size() > 0) {
			name = string(meta.sdefault[0].c_str());
			name_found = true;
		}
	}

	return name;
}


/*CONTAINER NodeTest
{
	NAME NodeTest;

	INCLUDE GVbase;
	SCALE_H;

	GROUP Obaselist
	{
		COLOR COLORDATA{EDITPORT; INPORT; CREATEPORT; SCALE_H; }
		CLOSURE OUTCLOSURE{EDITPORT; OUTPORT; PORTONLY; CREATEPORT; SCALE_H; }
	}


}*/




void WriteCppFile(DlShaderInfo* shaderinfo) {
	ofstream file;
	string shadername = string(shaderinfo->shadername().c_str());

	string classname = ClassName(shadername);
	string nicename = osl_utilities::FindMetaData(shaderinfo->metadata(), "niceName", shadername);
	fs::path dir = fs::current_path() / "source";
	fs::create_directories(dir);

	file.open(string(dir.string() + "//" + classname + ".cpp"));

	file << "//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit." << endl;
	file << endl;
	file << "#include \"c4d.h\"" << endl;
	file << "#include \"" << classname << ".h\"" << endl;
	file << "#include \"gvbase.h\"" << endl;
	file << "#include \"c4d_graphview.h\"" << endl;
	file << "#include <c4d_operatordata.h>" << endl;
	file << "#include \"IDs.h\"" << endl;

	//file << "#define DL_NODES_CLASS 1061282" << endl;
	file << endl;

	//Class definition
	file << "class " << classname << " : public  GvOperatorData {" << endl;
	file << "public:" << endl;
	file << "	static NodeData * Alloc(void) {" << endl;
	file << "		return NewObjClear(" << classname << "); " << endl;
	file << "	}" << endl;
	file << endl;
	file << "	virtual Bool iCreateOperator(GvNode * 	bn);" << endl;
	file << "};" << endl;

	file << endl;

	//Init function
	file << "Bool " << classname << "::iCreateOperator(GvNode * 	bn){" << endl;
	//file << "	BaseContainer* data = node->GetDataInstance();" << endl;
	//file << "	BaseContainer* data = op->GetOpContainerInstance();" << endl;
	//file << Indent(1) << "GvNode* op = (GvNode*)node;" << endl;
	file << Indent(1) << "BaseContainer* data = bn->GetOpContainerInstance();" << endl;
	file << endl;

	//for (int i = 0; i < params.size(); i++) {
	//	file << "	data->SetFloat(" << EnumName(params[i].name) << " , " << params[i].value << ");" << endl;
	//}
	long nparams = shaderinfo->nparams();

	for (long i = 0; i < nparams; i++) {
		auto param = shaderinfo->getparam(i);
		string param_name = ParamName(string(param->name.data()));//EnumName(string(param->name.data()));
		int type = param->type.elementtype;
		if (param->isoutput) { continue; } //Only set default values for input parameters

		int skip_init = osl_utilities::FindIntMetaData(param->metadata, "skip_init", 0);
		if (skip_init == 1) {
			continue;
		}

		if (type == NSITypeFloat  ) {
			string widget = osl_utilities::FindMetaData(param->metadata, "widget");

			if (widget == "maya_floatRamp") {
				file << "	{" << endl;
				//file << "//Spline: " << EnumName(param_name) << endl;

				file << "		AutoAlloc<SplineData> spline;" << endl;
				file << "		spline->MakeLinearSplineLinear(2);"<<endl;
				file << "		CustomSplineKnot* knot0 = spline->GetKnot(0);" << endl;
				file << "		knot0->vPos = Vector(0, 0, 0);" << endl;
				file << "		CustomSplineKnot* knot1 = spline->GetKnot(1);" << endl;
				file << "		knot1->vPos = Vector(1, 1, 0);" << endl;
				//file << "		spline->InsertKnot(0, 0);" << endl;
				//file << "		spline->InsertKnot(1, 1);" << endl;
				file << "		data->SetData("<<param_name<<", GeData(CUSTOMDATATYPE_SPLINE, *spline));" << endl;

				file << "	}" << endl;
			}
			else if (param->type.arraylen == 0) {
				float d = param->fdefault[0];
				file << "	data->SetFloat(" << EnumName(param_name) << " , " << d << ");" << endl;
			}
			//Handle other GUI elements here, e.g., ramp. See Ogers code!
		}
		else if (type == NSITypeInteger && param->type.arraylen == 0) {
			string widget = osl_utilities::FindMetaData(param->metadata, "widget");
			if (widget == "checkBox") {
				int d = param->idefault[0];
				bool val = (d != 0);
				file << "	data->SetBool(" << EnumName(param_name) << " , " << val << ");" << endl;
			}
			else if (widget == "mapper") {
				string options = osl_utilities::FindMetaData(param->metadata, "options");
				if (options != "") {
					vector<string> mapper_elements = SplitString(options, '|');
					vector<string> first_element_parts = SplitString(mapper_elements[0], ':');
					string first_element_name = first_element_parts[0];
					file << "	data->SetInt32(" << EnumName(param_name) << " , " << EnumName(param_name) << "_" << EnumName(first_element_name) << ");" << endl;

				}
			}
			else {
				int d = param->idefault[0];
				file << "	data->SetInt32(" << EnumName(param_name) << " , " << d << ");" << endl;
			}

		}
		else if (type == NSITypeString) {
			string widget = osl_utilities::FindMetaData(param->metadata, "widget");
			if (widget == "popup") {
				string options = osl_utilities::FindMetaData(param->metadata, "options");
				if (options != "") {
					vector<string> popup_elements = SplitString(options, '|');

					int default_option = -1;
					string default_string = string(param->sdefault[0].c_str());

					for (int j = 0; j < popup_elements.size(); j++) {
						if (popup_elements[j] == default_string) {
							default_option = j;
						}
					}
					if (default_option != -1) {
						file << "	data->SetInt32(" << EnumName(param_name) << ", " << EnumName(param_name) << "_" << EnumName(popup_elements[default_option]) << ");" << endl;

					}
				}
			}
			else {
				file << "	data->SetString(" << EnumName(param_name) << ", String(\"" << param->sdefault[0].c_str() << "\"));" << endl;
			}
		}
		else if (type == NSITypeColor ) {
			string widget = osl_utilities::FindMetaData(param->metadata, "widget");
			if (widget == "maya_colorRamp") {
				//file << Indent(1) << "ApplicationOutput(\"Color ramp init+\");" << endl;
			file << Indent(1) << "{" << endl;
				file << Indent(2) << "AutoAlloc<Gradient> gradient;" << endl;
				file << Indent(2) << "if (!gradient) return false;" << endl;
				file << Indent(2) << "GradientKnot k;" << endl;
				file << Indent(2) << "k.col = Vector(0, 0, 0);" << endl;
				file << Indent(2) << "k.pos = 0;" << endl;
				file << Indent(2) << "gradient->InsertKnot(k);" << endl;
				file << Indent(2) << "k.col = Vector(1, 1, 1);" << endl;
				file << Indent(2) << "k.pos = 1;" << endl;
				file << Indent(2) << "gradient->InsertKnot(k);" << endl;
				file << Indent(2) << "data->SetData("<< EnumName(param_name) <<", GeData(CUSTOMDATATYPE_GRADIENT, *gradient));" << endl;
				file << Indent(1) << "}" << endl;
	
				/*{
					using namespace osl_utilities;
					using namespace osl_utilities::ramp;
					const DlShaderInfo::Parameter* knots = nullptr;
					const DlShaderInfo::Parameter* interpolation = nullptr;
					const DlShaderInfo::Parameter* shared_interpolation = nullptr;
					std::string base_name;
					if (FindMatchingRampParameters(
						*shaderinfo,
						*param,
						knots,
						interpolation,
						shared_interpolation,
						base_name))
					{
						cout << "Found matching ramp parameters:" << endl;
						string colorname = (string(param->name.data()));
						string knotname=(string(knots->name.data()));
						string interpolationname= (string(interpolation->name.data()));
						cout << colorname << endl;
						cout << knotname << endl;
						cout << interpolationname<<endl;ar

						param.

					}*/
					

				//}


				/*for (long j = 0; j < nparams; j++) {
					auto param2 = shaderinfo->getparam(i);
					string param_name2 = EnumName(string(param->name.data()));
					if (param_name2 == param_name + EnumName(".value_Knots")) {
						cout << "Found knot parameter" << endl;
					}
					if (param_name2 == param_name + EnumName(".value_Knots")) {
						cout << "Found knot parameter" << endl;
					}
					//int type = param->type.elementtype;
				}*/
				/*{
					AutoAlloc<Gradient> gradient;
					if (!gradient) return false;
					GradientKnot k;
					k.col = Vector(1, 0, 0);
					k.pos = 0;
					gradient->InsertKnot(k);
					k.col = Vector(0, 1, 0);
					k.pos = 1;
					gradient->InsertKnot(k);

					data->SetData(COLOR_COLOR, GeData(CUSTOMDATATYPE_GRADIENT, *gradient));


				}*/
			}
			else {
				file << Indent(1) << "data->SetVector(" << EnumName(param_name) << " , " << "Vector(";
				file << param->fdefault[0] << ", " << param->fdefault[1] << ", " << param->fdefault[2] << "));" << endl;
			}
		}
		else if (type == NSITypeNormal || type == NSITypeVector || type == NSITypePoint) {
			file << Indent(1) << "data->SetVector(" << EnumName(param_name) << " , " << "Vector(";
			file << param->fdefault[0] << ", " << param->fdefault[1] << ", " << param->fdefault[2] << "));" << endl;
		}

	}

	file << endl;
	file << "	return  GvOperatorData::iCreateOperator(bn);" << endl;
	file << "}" << endl;

	file << endl;
	//Registration
	file << "Bool Register" << classname << "(const char* icon=\"dl_200.tif\", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){" << endl;
	file << "	return GvRegisterOperatorPlugin(" << IdName(shadername) << ", ";
	file << "\"" << nicename << "\"_s, ";
	file << "0, ";
	file << classname << "::Alloc, ";
	file << "\"" << shadername << "\"_s, ";
	file << "0, ";
	file << "DL_NODES_CLASS,";

	/*string nodegroup_id = "ID_GV_OPGROUP_TYPE_GENERAL";
	if (nodegroup == "Material") {
		nodegroup_id = "DL_MATERIAL_GROUP";
	}

	if (nodegroup == "Texture3D") {
		nodegroup_id = "DL_THREEDTEXTURE_GROUP";
	}

	if (nodegroup == "Texture2D") {
		nodegroup_id = "DL_TWODTEXTURE_GROUP";
	}

	if (nodegroup == "Utilities") {
		nodegroup_id = "DL_UTILITIES_GROUP";
	}*/

	file << "node_group, ";
	file << "0, ";
	file << "AutoBitmap(String(icon))); " << endl;
	file << "}" << endl;

	/*return GvRegisterOperatorPlugin(DL_NODE_TEST,
		"NodeTest"_s,
		0,
		NodeTest::Alloc,
		"NodeTest"_s,
		0,
		DL_NODES_CLASS,
		ID_GV_OPGROUP_TYPE_GENERAL,
		0,
		AutoBitmap("dl_200.tif"_s)
	);*/


	file.close();
}

void WriteTranslator(DlShaderInfo* shaderinfo, string shaderpath) {
	ofstream headerfile;
	ofstream sourcefile;
	string shadername = string(shaderinfo->shadername().c_str());

	string classname = ClassName(shadername) + "Translator";
	fs::path dir = fs::current_path() / "source";
	fs::create_directories(dir);

	headerfile.open(string(dir.string() + "//" + classname + ".h"));


	headerfile << "//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit." << endl;
	headerfile << endl;
	headerfile << "#pragma once" << endl;
	headerfile << "#include \"c4d.h\"" << endl;
	headerfile << "#include \"DL_TranslatorPlugin.h\"" << endl;
	headerfile << "#include \"DL_PluginManager.h\"" << endl;
	

	headerfile << endl;
	headerfile << "class " << classname << " : public DL_TranslatorPlugin {" << endl;;
	headerfile << "public:" << endl;
	headerfile << Indent(1) << "virtual void CreateNSINodes(const char* Handle," << endl;
	headerfile << Indent(2) << "const char* ParentTransformHandle," << endl;
	headerfile << Indent(2) << "BaseList2D* C4DNode," << endl;
	headerfile << Indent(2) << "BaseDocument* doc," << endl;
	headerfile << Indent(2) << "DL_SceneParser* parser);" << endl;
	headerfile << endl;
	headerfile << Indent(1) << "virtual void ConnectNSINodes(const char* Handle," << endl;
	headerfile << Indent(2) << "const char* ParentTransformHandle," << endl;
	headerfile << Indent(2) << "BaseList2D* C4DNode," << endl;
	headerfile << Indent(2) << "BaseDocument* doc," << endl;
	headerfile << Indent(2) << "DL_SceneParser* parser);" << endl;
	headerfile << endl;
	headerfile << Indent(1) << "virtual void SampleAttributes(DL_SampleInfo* info," << endl;
	headerfile << Indent(2) << "const char* Handle," << endl;
	headerfile << Indent(2) << "BaseList2D* C4DNode," << endl;
	headerfile << Indent(2) << "BaseDocument* doc," << endl;
	headerfile << Indent(2) << "DL_SceneParser* parser);" << endl;
	headerfile << endl;
	headerfile << Indent(1) << "static void RegisterAttributeNames(DL_PluginManager* PM);" << endl;
	headerfile << "};" << endl;



	headerfile.close();
	sourcefile.open(string(dir.string() + "//" + classname + ".cpp"));

	sourcefile << "#include \"" << classname << ".h\"" << endl;
	sourcefile << "#include \"" << ClassName(shadername) << ".h\"" << endl;
	sourcefile << "#include <c4d_graphview.h>" << endl;
	sourcefile << "#include \"IDs.h\"" << endl;
	sourcefile << "#include \"DL_utilities.h\"" << endl;
	sourcefile << "#include \"DL_TypeConversions.h\"" << endl;
	//sourcefile << "#include \"RampUtilities.h\"" << endl;
	sourcefile << "#include \"nsi.hpp\"" << endl;
	sourcefile << "#include \"delightenvironment.h\"" << endl;
	sourcefile << endl;

	sourcefile << "void " << classname << "::CreateNSINodes(const char* Handle," << endl;
	sourcefile << Indent(1) << "const char* ParentTransformHandle," << endl;
	sourcefile << Indent(1) << "BaseList2D* C4DNode," << endl;
	sourcefile << Indent(1) << "BaseDocument* doc," << endl;
	sourcefile << Indent(1) << "DL_SceneParser* parser){" << endl;

	//Create NSI nodes code here
	sourcefile << Indent(2) << "NSI::Context& ctx(parser->GetContext());" << endl;
	sourcefile << Indent(2) << "std::string shader_handle = (std::string) Handle;" << endl;
	sourcefile << Indent(2) << "ctx.Create(shader_handle, \"shader\");" << endl;

	sourcefile << Indent(2) << "const char* delightpath = DelightEnv::getDelightEnvironment();" << endl;
	sourcefile << Indent(2) << "std::string  shaderpath = std::string(delightpath) + (std::string)\"" << shaderpath << "\";" << endl;

	sourcefile << Indent(2) << "ctx.SetAttribute(shader_handle, NSI::StringArg(\"shaderfilename\", shaderpath));" << endl;

	long nparams = shaderinfo->nparams();

	for (long i = 0; i < nparams; i++) {
		auto param = shaderinfo->getparam(i);
		string param_name = string(param->name.data());
		string default_connection = osl_utilities::FindMetaData(param->metadata, "default_connection");
		//default_connection, "uvCoord"
		if (default_connection == "uvCoord") {
			{
				sourcefile << Indent(2) << "ctx.Connect(\"*uvCoord\", \"o_outUV\", shader_handle, \"" << param_name << "\" );" << endl;;
			}
		}

		if (param_name == "placementMatrix" && param->type.elementtype==NSITypeMatrix) {
			sourcefile << Indent(2) << "ctx.Connect(\"3dlfc4d::default_placement_matrix\", \"out\", shader_handle, \"" << param_name << "\" );" << endl;;
		}
	}
	sourcefile << "}" << endl;
	sourcefile << endl;

	sourcefile << "void " << classname << "::ConnectNSINodes(const char* Handle," << endl;
	sourcefile << Indent(1) << "const char* ParentTransformHandle," << endl;
	sourcefile << Indent(1) << "BaseList2D* C4DNode," << endl;
	sourcefile << Indent(1) << "BaseDocument* doc," << endl;
	sourcefile << Indent(1) << "DL_SceneParser* parser){" << endl;
	sourcefile << endl;



	sourcefile << Indent(1) << "NSI::Context& ctx(parser->GetContext());" << endl;
	sourcefile << Indent(1) << "GvNode* gvnode = (GvNode*)C4DNode;" << endl;
	sourcefile << Indent(1) << "std::string source_handle = std::string(Handle);" << endl;
	sourcefile << endl;

	sourcefile << Indent(1) << "//Clear any previous connections" << endl;
	sourcefile << Indent(1) << "ctx.Disconnect(source_handle, \".all\", \".all\", \".all\");" << endl;
	sourcefile << endl;

	sourcefile << Indent(1) << "GvPort* port;" << endl;
	sourcefile << Indent(1) << "GvDestination* destination;" << endl;
	sourcefile << Indent(1) << "for (int i = 0; i < gvnode->GetOutPortCount(); i++) {" << endl;
	sourcefile << Indent(2) << "port = gvnode->GetOutPort(i);" << endl;
	sourcefile << Indent(2) << "std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());" << endl;
	sourcefile << Indent(2) << "for (int j = 0; j < port->GetNrOfConnections(); j++) {" << endl;
	sourcefile << Indent(3) << "destination = port->GetOutgoing(j);" << endl;
	sourcefile << Indent(3) << "if (destination) {" << endl;
	sourcefile << Indent(4) << "std::string dest_handle = parser->GetHandleName(destination->node);" << endl;
	sourcefile << Indent(4) << "long destID = destination->node->GetOperatorID();" << endl;
	sourcefile << Indent(4) << "long destAttributeID = destination->port->GetMainID();" << endl;
	sourcefile << Indent(4) << "std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);" << endl;
	sourcefile << Indent(4) << "ctx.Disconnect(\".all\", \".all\", dest_handle, dest_attribute_name);" << endl;
	sourcefile << Indent(4) << "ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);" << endl;

	sourcefile << Indent(3) << "}" << endl;
	sourcefile << Indent(2) << "}" << endl;
	sourcefile << Indent(1) << "}" << endl;

	//long nparams = shaderinfo->nparams();

	/*for (long i = 0; i < nparams; i++) {
		auto param = shaderinfo->getparam(i);
		string param_name = string(param->name.data());
		string default_connection = osl_utilities::FindMetaData(param->metadata, "default_connection");
		//default_connection, "uvCoord"
		if (default_connection == "uvCoord") {
			{
				sourcefile << Indent(1) << "ctx.Connect(\"*uvCoord\", \"o_outUV\", source_handle, \"" << param_name << "\" );" << endl;;
			}
		}
	}*/
	sourcefile << "}" << endl;;
	sourcefile << endl;

	sourcefile << "void " << classname << "::SampleAttributes(DL_SampleInfo* info," << endl;
	sourcefile << Indent(1) << "const char* Handle," << endl;
	sourcefile << Indent(1) << "BaseList2D* C4DNode," << endl;
	sourcefile << Indent(1) << "BaseDocument* doc," << endl;
	sourcefile << Indent(1) << "DL_SceneParser* parser){" << endl;
	sourcefile << endl;

	//Sample attributes code here
	sourcefile << Indent(1) << "NSI::Context& ctx(parser->GetContext());" << endl;
	sourcefile << Indent(1) << "GvNode* gvnode = (GvNode*)C4DNode;" << endl;
	sourcefile << Indent(1) << "BaseContainer * data = gvnode->GetOpContainerInstance();" << endl;
	sourcefile << Indent(1) << "std::string shader_handle = (std::string) Handle;" << endl;
	sourcefile << endl;

	sourcefile << Indent(1) << "float f;" << endl;
	sourcefile << Indent(1) << "Vector v;" << endl;
	sourcefile << Indent(1) << "float v_data[3];" << endl;
	sourcefile << endl;


	//long nparams = shaderinfo->nparams();

	for (long i = 0; i < nparams; i++) {
		auto param = shaderinfo->getparam(i);
		string param_name = (string(param->name.data()));
		int type = param->type.elementtype;
		auto it = custom_translation_code.find(param_name);
		if (it != custom_translation_code.end()) {
			sourcefile << it->second;
			sourcefile << endl;
		}
		else if (type == NSITypeFloat) {
			string widget = osl_utilities::FindMetaData(param->metadata, "widget");
			if (widget == "maya_floatRamp") {
				sourcefile << Indent(2) << "{" << endl;

				{
					using namespace osl_utilities;
					using namespace osl_utilities::ramp;
					const DlShaderInfo::Parameter* knots = nullptr;
					const DlShaderInfo::Parameter* interpolation = nullptr;
					const DlShaderInfo::Parameter* shared_interpolation = nullptr;
					std::string base_name;
					if (FindMatchingRampParameters(
						*shaderinfo,
						*param,
						knots,
						interpolation,
						shared_interpolation,
						base_name))
					{
						string floatname = (string(param->name.data()));
						string knotname = (string(knots->name.data()));
						string interpolationname = (string(interpolation->name.data()));

						sourcefile << Indent(3) << "SplineData* spline = (SplineData*)data->GetCustomDataType(" << ParamName(param_name) << ", CUSTOMDATATYPE_SPLINE);" << endl;
						sourcefile << endl;
						sourcefile << Indent(3) << "int knot_count;" << endl;
						sourcefile << Indent(3) << "std::vector<float> knots;" << endl;
						sourcefile << Indent(3) << "std::vector<float> values;" << endl;
						sourcefile << Indent(3) << "std::vector<int> interpolations;" << endl;
						sourcefile << Indent(3) << "FillFloatRampNSIData(spline, doc, knots, values, interpolations, knot_count);" << endl;

						sourcefile << endl;

						sourcefile << Indent(3) << "NSI::Argument arg_knot_values(\"" << floatname << "\");" << endl;
						sourcefile << Indent(3) << "arg_knot_values.SetArrayType(NSITypeFloat, knot_count);" << endl;
						sourcefile << Indent(3) << "arg_knot_values.SetValuePointer((void*)&values[0]);" << endl;
						sourcefile << Indent(3) << "ctx.SetAttribute(shader_handle, (arg_knot_values));" << endl;
						sourcefile << endl;

						sourcefile << Indent(3) << "NSI::Argument arg_knot_position(\"" << knotname << "\");" << endl;
						sourcefile << Indent(3) << "arg_knot_position.SetArrayType(NSITypeFloat, knot_count);" << endl;
						sourcefile << Indent(3) << "arg_knot_position.SetValuePointer((void*)&knots[0]);" << endl;
						sourcefile << Indent(3) << "ctx.SetAttribute(shader_handle, (arg_knot_position));" << endl;
						sourcefile << endl;

						sourcefile << Indent(3) << "NSI::Argument arg_knot_interpolation(\"" << interpolationname << "\");" << endl;
						sourcefile << Indent(3) << "arg_knot_interpolation.SetArrayType(NSITypeInteger, knot_count);" << endl;
						sourcefile << Indent(3) << "arg_knot_interpolation.SetValuePointer((void*)&interpolations[0]);" << endl;
						sourcefile << Indent(3) << "ctx.SetAttribute(shader_handle, (arg_knot_interpolation));" << endl;
						sourcefile << endl;



					}


				}

				sourcefile << Indent(2) << "}" << endl;
				sourcefile << endl;
			}
			else if (param->type.arraylen == 0) {
				sourcefile << Indent(2) << "f = (float) data->GetFloat(" << ParamName(param_name) << ");" << endl;
				sourcefile << Indent(2) << "ctx.SetAttribute(shader_handle, NSI::FloatArg(\"" << param_name << "\", f));" << endl;
				sourcefile << endl;
			}
			
		}
		else if (type == NSITypeInteger) {
			//sourcefile << Indent(2) << "{" << endl;

			string widget = osl_utilities::FindMetaData(param->metadata, "widget");
			if (widget == "checkBox") {
				sourcefile << Indent(2) << "{" << endl;
				sourcefile << Indent(3) << "int value = (int)data->GetBool(" << ParamName(param_name) << ");" << endl;
				sourcefile << Indent(3) << "ctx.SetAttribute(shader_handle, NSI::IntegerArg(\"" << param_name << "\", value));" << endl;
				sourcefile << Indent(2) << "}" << endl << endl;
			}
			else if (widget == "mapper") {
				sourcefile << Indent(2) << "{" << endl;
				sourcefile << Indent(3) << "Int32 option = data->GetInt32(" << ParamName(param_name) << ");" << endl;
				string options = osl_utilities::FindMetaData(param->metadata, "options");
				if (options != "") {
					vector<string> mapper_elements = SplitString(options, '|');
					for (int j = 0; j < mapper_elements.size(); j++) {
						vector<string> parts = SplitString(mapper_elements[j], ':');
						string element_name = parts[0];
						int element_id = atoi(parts[1].c_str());
						sourcefile << Indent(3);
						if (j != 0) {
							sourcefile << "else ";
						}
						//file << Indent(1) << param_name << "_" << EnumName(popup_elements[j]) << "," << endl;
						sourcefile << "if(option == " << ParamName(param_name) << "_" << EnumName(element_name) << "){" << endl;
						sourcefile << Indent(4) << "ctx.SetAttribute(shader_handle, NSI::IntegerArg(\"" << param_name << "\", " << element_id << "));" << endl;
						sourcefile << Indent(3) << "}" << endl;
					}

				}
				sourcefile << Indent(2) << "}" << endl << endl;
			}
			else if (param->type.arraylen == 0) {
				sourcefile << Indent(2) << "{" << endl;
				sourcefile << Indent(3) << "int value = (int)data->GetInt32(" << ParamName(param_name) << ");" << endl;
				sourcefile << Indent(3) << "ctx.SetAttribute(shader_handle, NSI::IntegerArg(\"" << param_name << "\", value));" << endl;
				sourcefile << Indent(2) << "}" << endl << endl;
			}


		}
		else if (type == NSITypeString) {
			//file << "	data->SetString(" << EnumName(param_name) << ", \"" << param->sdefault[0].c_str() << "\");" << endl;
			sourcefile << Indent(2) << "{" << endl;

			string widget = osl_utilities::FindMetaData(param->metadata, "widget");

			if (widget == "filename") {
				sourcefile << Indent(3) << "Filename fn = data->GetFilename(" << ParamName(param_name) << ");" << endl;
				sourcefile << Indent(3) << "String s = fn.GetString();" << endl;
				sourcefile << Indent(3) << "ctx.SetAttribute(shader_handle, NSI::StringArg(\"" << param_name << "\", StringToStdString(s)));" << endl;
			}
			else if (widget == "popup") {
				sourcefile << Indent(3) << "Int32 option = data->GetInt32(" << ParamName(param_name) << ");" << endl;
				string options = osl_utilities::FindMetaData(param->metadata, "options");
				if (options != "") {
					vector<string> popup_elements = SplitString(options, '|');
					for (int j = 0; j < popup_elements.size(); j++) {
						sourcefile << Indent(3);
						if (j != 0) {
							sourcefile << "else ";
						}
						//file << Indent(1) << param_name << "_" << EnumName(popup_elements[j]) << "," << endl;
						sourcefile << "if(option == " << ParamName(param_name) << "_" << EnumName(popup_elements[j]) << "){" << endl;
						sourcefile << Indent(4) << "ctx.SetAttribute(shader_handle, NSI::StringArg(\"" << param_name << "\", \"" << popup_elements[j] << "\"));" << endl;
						sourcefile << Indent(3) << "}" << endl;
					}

				}
			}
			else {
				sourcefile << Indent(3) << "String s = data->GetString(" << ParamName(param_name) << ");" << endl;
				sourcefile << Indent(3) << "ctx.SetAttribute(shader_handle, NSI::StringArg(\"" << param_name << "\", StringToStdString(s)));" << endl;
			}
			sourcefile << Indent(2) << "}" << endl << endl;

		}
		else if (type == NSITypeColor) {
			string widget = osl_utilities::FindMetaData(param->metadata, "widget");
			if (widget == "maya_colorRamp") {
				sourcefile << Indent(2) << "{" << endl;

				{
					using namespace osl_utilities;
					using namespace osl_utilities::ramp;
					const DlShaderInfo::Parameter* knots = nullptr;
					const DlShaderInfo::Parameter* interpolation = nullptr;
					const DlShaderInfo::Parameter* shared_interpolation = nullptr;
					std::string base_name;
					if (FindMatchingRampParameters(
						*shaderinfo,
						*param,
						knots,
						interpolation,
						shared_interpolation,
						base_name))
					{
						string colorname = (string(param->name.data()));
						string knotname=(string(knots->name.data()));
						string interpolationname= (string(interpolation->name.data()));
												
						sourcefile << Indent(3) << "Gradient* gradient = (Gradient*)data->GetCustomDataType("<<ParamName(param_name) <<", CUSTOMDATATYPE_GRADIENT );" << endl;
						sourcefile << endl;
						sourcefile << Indent(3) << "int knot_count;" << endl;
						sourcefile << Indent(3) << "std::vector<float> knots;" << endl;
						sourcefile << Indent(3) << "std::vector<float> colors;"<<endl;
						sourcefile << Indent(3) << "std::vector<int> interpolations;" << endl;
						sourcefile << Indent(3) << "FillColorRampNSIData(gradient, doc, knots, colors, interpolations, knot_count);" << endl;
					
						sourcefile << endl;

						sourcefile << Indent(3) << "NSI::Argument arg_knot_color(\""<< colorname<<"\");" << endl;
						sourcefile << Indent(3) << "arg_knot_color.SetArrayType(NSITypeColor, knot_count);" << endl;
						sourcefile << Indent(3) << "arg_knot_color.SetValuePointer((void*)&colors[0]);" << endl;
						sourcefile << Indent(3) << "ctx.SetAttribute(shader_handle, (arg_knot_color));" << endl;
						sourcefile << endl;

						sourcefile << Indent(3) << "NSI::Argument arg_knot_position(\"" << knotname << "\");" << endl;
						sourcefile << Indent(3) << "arg_knot_position.SetArrayType(NSITypeFloat, knot_count);" << endl;
						sourcefile << Indent(3) << "arg_knot_position.SetValuePointer((void*)&knots[0]);" << endl;
						sourcefile << Indent(3) << "ctx.SetAttribute(shader_handle, (arg_knot_position));" << endl;
						sourcefile << endl;

						sourcefile << Indent(3) << "NSI::Argument arg_knot_interpolation(\"" << interpolationname << "\");" << endl;
						sourcefile << Indent(3) << "arg_knot_interpolation.SetArrayType(NSITypeInteger, knot_count);" << endl;
						sourcefile << Indent(3) << "arg_knot_interpolation.SetValuePointer((void*)&interpolations[0]);" << endl;
						sourcefile << Indent(3) << "ctx.SetAttribute(shader_handle, (arg_knot_interpolation));" << endl;
						sourcefile << endl;



					}


				}

				sourcefile << Indent(2) << "}" << endl;
				sourcefile << endl;
			}
			else if (param->type.arraylen == 0) {
				sourcefile << Indent(1) << "{" << endl;
				sourcefile << Indent(2) << "v = toLinear(data->GetVector(" << ParamName(param_name) << "), doc);" << endl;

				sourcefile << Indent(2) << "v_data[0] = (float)v.x;" << endl;
				sourcefile << Indent(2) << "v_data[1] = (float)v.y;" << endl;
				sourcefile << Indent(2) << "v_data[2] = (float)v.z;" << endl;

				sourcefile << Indent(2) << "ctx.SetAttribute(shader_handle, NSI::ColorArg(\"" << param_name << "\", &v_data[0]));" << endl;
				sourcefile << Indent(1) << "}" << endl;
				sourcefile << endl;
			}
			//file << "	data->SetVector(" << EnumName(param_name) << " , " << "Vector(";
			//file << param->fdefault[0] << ", " << param->fdefault[1] << ", " << param->fdefault[2] << "));" << endl;
		}
		else if (type == NSITypeMatrix) {
			sourcefile << Indent(1) << "{" << endl;

			sourcefile << Indent(2) << "Matrix m = data->GetMatrix(" << ParamName(param_name) << ");" << endl;
			sourcefile << Indent(2) << "std::vector<float> mdata = MatrixToNSIMatrix(m);" << endl;
			sourcefile << Indent(2) << "NSI::Argument matrix_arg(\"" << param_name << "\");" << endl;
			sourcefile << Indent(2) << "matrix_arg.SetType(NSITypeMatrix);" << endl;
			sourcefile << Indent(2) << "matrix_arg.SetValuePointer((void*)&mdata[0]);" << endl;

			sourcefile << Indent(2) << "ctx.SetAttribute(shader_handle, matrix_arg);" << endl;

			sourcefile << Indent(1) << "}" << endl;

			sourcefile << endl;
			//file << "	data->SetVector(" << EnumName(param_name) << " , " << "Vector(";
			//file << param->fdefault[0] << ", " << param->fdefault[1] << ", " << param->fdefault[2] << "));" << endl;
		}

	}

	/*	}
		else if (type == NSITypeInteger) {
			int d = param->idefault[0];
			file << "	data->SetInt32(" << EnumName(param_name) << " , " << d << ");" << endl;

			//Handle other GUI elements here. See Ogers code!

		}
		else if (type == NSITypeString) {
			file << "	data->SetString(" << EnumName(param_name) << ", \"" << param->sdefault[0].c_str() << "\");" << endl;
		}
		else if (type == NSITypeColor || type == NSITypeNormal || type == NSITypeVector || type == NSITypePoint) {
			file << "	data->SetVector(" << EnumName(param_name) << " , " << "Vector(";
			file << param->fdefault[0] << ", " << param->fdefault[1] << ", " << param->fdefault[2] << "));" << endl;
		}

	}*/

	sourcefile << "}" << endl;
	sourcefile << endl;

	sourcefile << "void  " << classname << "::RegisterAttributeNames(DL_PluginManager* PM){" << endl;
	shaderinfo->nparams();

	string ID_enum = IdName(shadername);
	for (long i = 0; i < nparams; i++) {
		auto param = shaderinfo->getparam(i);
		string param_name = (string(param->name.data()));
		sourcefile << Indent(1) << "PM->RegisterAttributeName(" << ID_enum << ", " << ParamName(param_name) << ", \"" << param_name << "\");" << endl;

	}

	sourcefile << "}" << endl;

	sourcefile.close();
}



void WriteHeaderFile(DlShaderInfo* shaderinfo) {
	ofstream file;

	string shadername = string(shaderinfo->shadername().c_str());
	string classname = ClassName(shadername);

	fs::path dir = fs::current_path() / "res" / "description";
	fs::create_directories(dir);
	file.open(dir.string() + "//" + classname + ".h");

	//TODO: Handle groups, "pages" in 3delight OSL metadata. Can be nested! 


	file << "#pragma once" << endl;
	file << endl;

	file << "enum {" << endl;


	long nparams = shaderinfo->nparams();


	//Groups
	set<string> groups;

	for (long i = 0; i < nparams; i++) {
		auto param = shaderinfo->getparam(i);
		auto metadata = param->metadata;

		bool page_found = false;
		string page;
		for (long j = 0; j < metadata.size() && !page_found; j++) {
			DlShaderInfo::Parameter meta = metadata[j];
			if (meta.name == "page" && meta.sdefault.size() > 0) {
				page = string(meta.sdefault[0].c_str());
				vector<string> pages = SplitString(page, '.');
				for (int k = 0; k < pages.size(); k++) {
					groups.insert(pages[k]);
				}

				//groups.insert(page);
				page_found = true;
			}
		}

	}

	//cout << groups.size() << endl;

	//for (auto it = groups.begin(); it != groups.end(); it++) {
		//cout << EnumName(*it) << endl;
	//}

	//Parameters

	for (long i = 0; i < nparams; i++) {
		auto param = shaderinfo->getparam(i);
		string param_name = ParamName(string(param->name.data()));






		int type = param->type.elementtype;


		
		file << Indent(1) << param_name;

		if (i == 0) {
			file << "=1000";
		}
		if ((i + 1 < nparams) || !groups.empty()) {

			file << ",";
		}
		file << endl;

		if (type == NSITypeString) {
			string widget = osl_utilities::FindMetaData(param->metadata, "widget");
			if (widget == "popup") {
				string options = osl_utilities::FindMetaData(param->metadata, "options");
				if (options != "") {
					vector<string> popup_elements = SplitString(options, '|');
					for (int j = 0; j < popup_elements.size(); j++) {
						file << Indent(1) << param_name << "_" << EnumName(popup_elements[j]) << "," << endl;
					}

				}
			}
		}
		else if (type == NSITypeInteger) {
			string widget = osl_utilities::FindMetaData(param->metadata, "widget");
			if (widget == "mapper") {
				string options = osl_utilities::FindMetaData(param->metadata, "options");
				if (options != "") {
					vector<string> mapper_elements = SplitString(options, '|');
					for (int j = 0; j < mapper_elements.size(); j++) {
						vector<string> parts = SplitString(mapper_elements[j], ':');
						string element_name = parts[0];
						file << Indent(1) << param_name << "_" << EnumName(element_name) << "," << endl;
					}


				}
			}
		}
		//int type = param->type.elementtype;

	}

	int counter = 0;
	for (auto it = groups.begin(); it != groups.end(); it++) {
		file << Indent(1) << GroupName(*it);
		if ((counter + 1 < groups.size())) {
			file << ",";
		}
		file << endl;
		counter++;
	}


	//file << "	" << EnumName(shadername) << "=1000";
	/*if (!params.empty()) {
		file << ",";
	}
	file << endl;


	for (int i = 0; i < params.size(); i++) {
		file << "	" << EnumName(params[i].name);
		if (i + 1 < params.size()) {
			file << ",";
		}
		file << endl;
	}*/
	file << "};" << endl;
}

void WriteParameterDescription(DlShaderInfo* shaderinfo, int i, ofstream& file, int indent) {
	auto param = shaderinfo->getparam(i);
	string param_name = ParamName(string(param->name.data()));

	osl_utilities::ParameterMetaData meta;
	osl_utilities::GetParameterMetaData(meta, param->metadata);

	bool hidden = (osl_utilities::FindIntMetaData(param->metadata, "hidden", 0) != 0);

	//if (hidden) {
		//cout << "Hidden" << endl;
	//}

	string porttype = "INPORT";
	if (param->isoutput) {
		porttype = "OUTPORT";
	}

	if (param->isclosure) {
		file << Indent(indent) << "CLOSURE " << param_name << "{ EDITPORT; " << porttype << "; PORTONLY;";
		if (param->isoutput) {
			file << " CREATEPORT;";
		}
		/*if (hidden) {
			file << " HIDDEN;" << endl;
		}*/
		file << " SCALE_H;}" << endl;
	}
	else {


		int type = param->type.elementtype;

		if (type == NSITypeFloat ) {
			string widget = osl_utilities::FindMetaData(param->metadata, "widget");

			if (widget == "maya_floatRamp") {
				file << Indent(indent) << "SPLINE " << param_name << "{X_MIN 0; X_MAX 1; EDITPORT; " << porttype << "; }" << endl;
			}
			else if (param->type.arraylen == 0) {
				file << Indent(indent) << "REAL " << param_name << "{EDITPORT; " << porttype << "; ";
				osl_utilities::ParameterMetaData meta;
				osl_utilities::GetParameterMetaData(meta, param->metadata);

				//if ((meta.m_fmax != nullptr && meta.m_fmin != nullptr) || meta.m_slider_fmax != nullptr || meta.m_slider_fmin != nullptr) {
				//	file << "CUSTOMGUI REALSLIDER; ";
				//}
				file << "CUSTOMGUI REALSLIDER; ";

				/*if (meta.m_slider_fmin != nullptr && meta.m_slider_fmax != nullptr) {
					file << "CUSTOMGUI REALSLIDER; ";
					file << "MINSLIDER " << *(meta.m_slider_fmin) << "; ";
					file << "MAXSLIDER " << *(meta.m_slider_fmax) << "; ";
					file << "STEP " << (*(meta.m_slider_fmax) - *(meta.m_slider_fmin)) / double(100) << "; " << endl;

				}*/

				float slider_min = 0;

				if (meta.m_slider_fmin != nullptr) {
					slider_min = *(meta.m_slider_fmin);
				}
				file << "MINSLIDER " << slider_min << "; ";

				float slider_max = 1;
				if (meta.m_slider_fmax != nullptr) {
					slider_max = *(meta.m_slider_fmax);
				}
				file << "MAXSLIDER " << slider_max << "; ";

				if (meta.m_fmin != nullptr) {
					file << "MIN " << *(meta.m_fmin) << "; ";
				}

				if (meta.m_fmax != nullptr) {
					file << "MAX " << *(meta.m_fmax) << "; ";
				}

				float step = 0.01;
				if (meta.m_slider_fmax != nullptr && meta.m_slider_fmin != nullptr) {
					step = (*(meta.m_slider_fmax) - *(meta.m_slider_fmin)) / double(100);
					//file << "STEP " << (*(meta.m_slider_fmax) - *(meta.m_slider_fmin)) / double(100) << "; ";
				}
				else if (meta.m_fmax != nullptr && meta.m_fmin != nullptr) {
					step = (*(meta.m_fmax) - *(meta.m_fmin)) / double(100);
					//file << "STEP " << (*(meta.m_fmax) - *(meta.m_fmin)) / double(100) << "; ";
				}
				file << "STEP " << step << "; ";

				if (param->isoutput) {
					file << " CREATEPORT;";
				}

				if (widget == "null") {
					file << "PORTONLY; ";
				}
				file << "}" << endl;
			}
		}
		if (type == NSITypeInteger && param->type.arraylen == 0) {
			string widget = osl_utilities::FindMetaData(param->metadata, "widget");
			if (widget == "checkBox") {
				file << Indent(indent) << "BOOL " << param_name << "{EDITPORT; " << porttype << ";}"<<endl;
			}
			else if (widget == "mapper") {
				file << Indent(indent) << "LONG " << param_name << "{ CYCLE{";

				string options = osl_utilities::FindMetaData(param->metadata, "options");
				if (options != "") {
					vector<string> mapper_elements = SplitString(options, '|');
					for (int j = 0; j < mapper_elements.size(); j++) {
						vector<string> parts = SplitString(mapper_elements[j], ':');
						string element_name = parts[0];
						file << param_name << "_" << EnumName(element_name) << "; ";
					}

				}

				file << "}; EDITPORT; " << porttype << ";}" << endl;
			}
			else {
				file << Indent(indent) << "LONG " << param_name << "{EDITPORT; " << porttype << "; ";
				//if (param->isoutput) {
				//	file << " CREATEPORT;";
				//}
				file << "}" << endl;
			}
		}
		if (type == NSITypeColor) {
			string widget = osl_utilities::FindMetaData(param->metadata, "widget");
			if (param->type.arraylen == 0) {
				file << Indent(indent) << "COLOR " << param_name << "{EDITPORT; " << porttype << "; ";
				if (param->isoutput) {
					file << " CREATEPORT;";
				}
				if (widget == "null") {
					file << " PORTONLY; ";
				}
				file << "}" << endl;
			}
			else{
				
				if (widget == "maya_colorRamp") {
					//cout << "EXPORT GRADIENT HERE" << endl << endl;
					file << Indent(indent) << "GRADIENT " << param_name << "{COLOR; EDITPORT; " << porttype << "; ";
					if (param->isoutput) {
						file << " CREATEPORT;";
					}
					file << "}" << endl;
				}
			}
		}
		if (type == NSITypeMatrix && param->type.arraylen == 0) {
			file << Indent(indent) << "MATRIX " << param_name << "{EDITPORT; PORTONLY; " << porttype << "; ";
			if (param->isoutput) {
				file << " CREATEPORT;";
			}
			file << "}" << endl;
		}
		if (type == NSITypeString && param->type.arraylen == 0) {
			string widget = osl_utilities::FindMetaData(param->metadata, "widget");
			//cout << widget << endl;
			if (widget == "filename") {
				file << Indent(indent) << "FILENAME " << param_name << "{EDITPORT; " << porttype << "; " << "}" << endl;
			}
			else if (widget == "popup") {
				//file << Indent(indent) << "GePrint(\"Mapper: \" + String(\""<< param_name << "\"));" << endl;
				//cout << "Popup: " << param_name << endl;
				//cout << "Options: "<< osl_utilities::FindMetaData(param->metadata, "options")<<endl;
				file << Indent(indent) << "LONG " << param_name << "{ CYCLE{";

				string options = osl_utilities::FindMetaData(param->metadata, "options");
				if (options != "") {
					vector<string> popup_elements = SplitString(options, '|');
					for (int j = 0; j < popup_elements.size(); j++) {
						file << param_name << "_" << EnumName(popup_elements[j]) << "; ";
					}

				}

				file << "}; EDITPORT; " << porttype << ";}" << endl;
			}
			else {
				file << Indent(indent) << "STRING " << param_name << "{EDITPORT; " << porttype << "; ";
				if (widget == "null") {
					file << "PORTONLY; ";
				}
				if (param->isoutput) {
					file << " CREATEPORT;";
				}
				file << "}" << endl;
			}
		}
	}
}

void WriteResourceFiles(DlShaderInfo* shaderinfo) {

	string shadername = string(shaderinfo->shadername().c_str());
	string classname = ClassName(shadername);

	fs::path dir = fs::current_path() / "res" / "description";
	fs::create_directories(dir);

	ofstream file;



	//Write res file
	file.open(dir.string() + "//" + classname + ".res");
	file << "CONTAINER " << shadername << std::endl;
	file << "{" << endl;
	file << "	NAME " << shadername << ";" << endl << endl;
	file << "	INCLUDE GVbase;" << endl;
	int indent = 1;
	//file << "	GROUP ID_GVPROPERTIES {" << endl;


	long nparams = shaderinfo->nparams();

	ParentGroup.WriteDescriptions(shaderinfo, file, 1);

	for (long i = 0; i < nparams; i++) {
		//WriteParameterDescription(shaderinfo, i, file);
		/*auto param = shaderinfo->getparam(i);
		string param_name = EnumName(string(param->name.data()));

		string porttype = "INPORT";
		if (param->isoutput) {
			porttype = "OUTPORT";
		}

		if (param->isclosure) {
			file << Indent(2) << "CLOSURE " << param_name << "{ EDITPORT; " << porttype << "; PORTONLY;";
			if (param->isoutput) {
				file << " CREATEPORT;";
			}
			file<<" SCALE_H;}" << endl;
		}
		else {


			int type = param->type.elementtype;

			if (type == NSITypeFloat && param->type.arraylen==0) {
				file << Indent(2) << "REAL " << param_name << "{EDITPORT; " << porttype << ";";
				if (param->isoutput) {
					file << " CREATEPORT;";
				}
				file << "}" << endl;
			}
			if (type == NSITypeColor && param->type.arraylen == 0) {

				file << Indent(2) << "COLOR " << param_name << "{EDITPORT; " << porttype<<"; ";
				if (param->isoutput) {
					file << " CREATEPORT;";
				}
				file<< "}" << endl;
			}
		}*/
	}
	//COLOR COLORDATA{EDITPORT; INPORT; CREATEPORT; SCALE_H; }
	//CLOSURE OUTCLOSURE{EDITPORT; OUTPORT; PORTONLY; CREATEPORT; SCALE_H; }
	//file << Indent(1) << "}" << endl;

	file << "}" << endl;
	file.close();

	//Write str file
	dir = fs::current_path() / "res" / "strings_us" / "description";
	fs::create_directories(dir);
	file.open(dir.string() + "//" + classname + ".str");

	file << "STRINGTABLE " << shadername << endl;
	file << "{" << std::endl;
	file << Indent(1) << shadername << " \"" << GetNiceName(shaderinfo) << "\";" << endl;

	osl_utilities::ParameterMetaData m_data;

	for (long i = 0; i < nparams; i++) {
		auto param = shaderinfo->getparam(i);
		osl_utilities::GetParameterMetaData(m_data, param->metadata);
		string page = osl_utilities::FindMetaData(param->metadata, "page");
		vector<string> pages = SplitString(page, '.');
		
		string label = osl_utilities::FindMetaData(param->metadata, "label", param->name.data());
	
		string widget = osl_utilities::FindMetaData(param->metadata, "widget");

		//For ramp parameters, the label may defined for any of the corresponding ramp parameters, so we search them all
		if ( (widget == "maya_colorRamp") || (widget == "maya_floatRamp")) {
			using namespace osl_utilities;
			using namespace osl_utilities::ramp;
			
			const DlShaderInfo::Parameter* knots = nullptr;
			const DlShaderInfo::Parameter* interpolation = nullptr;
			const DlShaderInfo::Parameter* shared_interpolation = nullptr;
			std::string base_name;
			if (FindMatchingRampParameters(*shaderinfo, *param, knots, interpolation, shared_interpolation, base_name)) {
				string tmplabel = osl_utilities::FindMetaData(param->metadata, "label", param->name.data());
				if (tmplabel != param->name.data()) {
					label = tmplabel;
				}

				tmplabel = osl_utilities::FindMetaData(knots->metadata, "label", knots->name.data());
				if (tmplabel != knots->name.data()) {
					label = tmplabel;
				}

				tmplabel = osl_utilities::FindMetaData(interpolation->metadata, "label", interpolation->name.data());
				if (tmplabel != interpolation->name.data()) {
					label = tmplabel;
				}

			}
		
		}



		file << Indent(1) << ParamName(string(param->name.data()));
		if (page != "") {
			file << " \"" << pages.back() << " " << label << "\"";
		}
		file << " \"" << label << "\";" << endl;

		string options = osl_utilities::FindMetaData(param->metadata, "options");

		int type = param->type.elementtype;

		if (type == NSITypeString) {
			string param_enum = ParamName(string(param->name.data()));
			string widget = osl_utilities::FindMetaData(param->metadata, "widget");
			if (widget == "popup") {
				string options = osl_utilities::FindMetaData(param->metadata, "options");
				if (options != "") {
					vector<string> popup_elements = SplitString(options, '|');
					for (int j = 0; j < popup_elements.size(); j++) {
						file << Indent(1) << param_enum << "_" << EnumName(popup_elements[j]) << " \"" << popup_elements[j] << "\";" << endl;
					}

				}
			}
		}
		else if (type == NSITypeInteger) {
			string param_enum = ParamName(string(param->name.data()));
			string widget = osl_utilities::FindMetaData(param->metadata, "widget");
			if (widget == "mapper") {
				string options = osl_utilities::FindMetaData(param->metadata, "options");
				if (options != "") {
					vector<string> mapper_elements = SplitString(options, '|');
					for (int j = 0; j < mapper_elements.size(); j++) {
						vector<string> parts = SplitString(mapper_elements[j], ':');
						string element_name = parts[0];
						file << Indent(1) << param_enum << "_" << EnumName(element_name) << " \"" << element_name << "\";" << endl;
					}

				}
			}
		}

		//<< " \"" << string(m_data.m_label) << "\" " << "\"" << "groupname" << " " << string(m_data.m_label) << "\"" << ";" << endl;
	}


	//Groups
	set<string> groups;

	for (long i = 0; i < nparams; i++) {
		auto param = shaderinfo->getparam(i);
		auto metadata = param->metadata;

		bool page_found = false;
		string page;
		for (long j = 0; j < metadata.size() && !page_found; j++) {
			DlShaderInfo::Parameter meta = metadata[j];
			if (meta.name == "page" && meta.sdefault.size() > 0) {
				page = string(meta.sdefault[0].c_str());
				vector<string> pages = SplitString(page, '.');
				for (int k = 0; k < pages.size(); k++) {
					groups.insert(pages[k]);
				}

				//groups.insert(page);
				page_found = true;
			}
		}

	}

	int counter = 0;
	for (auto it = groups.begin(); it != groups.end(); it++) {
		//file << "GROUP_" << EnumName(*it);

		file << Indent(1) << GroupName(*it) << " \"" << *it << "\";" << endl;
	}
	file << "}" << endl;

	file.close();
}

