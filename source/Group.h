#pragma once
#include "ShaderQuery.h"
#include <vector>
using namespace std;

class Group {
public:
	string name;
	vector<Group> subgroups;
	vector<int> parameters;

	void InsertInSubgroup(int parameter, vector<string>& subgroup_name);
	void WriteDescriptions(DlShaderInfo* shaderinfo, ofstream& file, int indent = 0);
	void AddParameter(int p);
};