/*
A command line tool for creating c4d plugin code and resource files for 3Delight for c4d, from compiled OSL shaders
*/

//s#include "Parameter.h"
#define _CRT_SECURE_NO_WARNINGS
#include "ShaderQuery.h"
#include "delightenvironment.h"
#include "nsi.h"
#include "Group.h"
#include<iostream>
#include <string>
#include <fstream>
#include <filesystem>
#include <algorithm>
#include <set>
#include <map>
#include <cctype>
#include <sstream>
#include <vector>
#include "CodeGen.h"
using namespace std;

namespace fs = std::filesystem;
using namespace std;

extern map<string, string> custom_translation_code;


int main(int argc, char *argv[]) {


	/*Group g;
	g.name = "ID_GV_PROPERTIES";

	string path = "BASE.DIFFUSE";
	vector<string> paths = SplitString(path, '.');
	std::reverse(paths.begin(), paths.end());
	g.InsertInSubgroup(3, paths);

	path = "COATING";
	paths = SplitString(path, '.');
	std::reverse(paths.begin(), paths.end());
	g.InsertInSubgroup(2, paths);

	path = "BASE.SPECULAR";
	paths = SplitString(path, '.');
	std::reverse(paths.begin(), paths.end());
	g.InsertInSubgroup(5, paths);*/

	//g.Write(-1);

	//map<string, string> custom_translation_code;
	

	if (argc > 2) {
		for (int i = 2; i < argc; i++) {
			filesystem::path p(argv[i]);
			string param = p.stem().string();
			//cout << "Filename: " << p.stem().string()<< endl;

			ifstream f(argv[i]); //taking file as inputstream
			std::ostringstream ss;
			ss << f.rdbuf();
			string code= ss.str();

			custom_translation_code[param] = code;
			//cout << "File: " << argv[i] << endl;
			//cout << "Code: " << endl;
			//cout << code << endl;
		}
	}

	Group parent_group;
	parent_group.name = "ID_GV_PROPERTIES";
	for (int i = 1; i <2; i++) {
		string shaderpath(argv[i]);
		const char* delightpath = DelightEnv::getDelightEnvironment();
		string full_shaderpath = string(delightpath) + shaderpath;
		//cout << "Shader #"<<i<<": "<<shaderpath << endl;
		DlShaderInfo* shaderinfo = DlGetShaderInfo(full_shaderpath.c_str());
		
		if (shaderinfo) { // Only generate files if the OSL shader was succcesfully loaded
			GetGroups(shaderinfo);
			WriteResourceFiles(shaderinfo);
			WriteHeaderFile(shaderinfo);
			WriteCppFile(shaderinfo);
			WriteTranslator(shaderinfo, shaderpath);
		}
		else {
			cout << "Could not load compiled OSL shader: " << endl;
			cout << shaderpath << endl;
			return 0;
		}

		
	}

	return 0;
}